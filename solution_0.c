
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define ARRAYNUM 5000

float ret;

float kyori(float a,float b);


float kyori(float a,float b){  /*2点間の距離を計算*/
  ret=(float)sqrt((double)a*a+b*b);
  return ret;
  }

int main(){
  FILE *fp;
  char *filename="input_2.csv";
  char buffer[256];
  char *tp1,*tp2;
  int i=0,j;
  float x[ARRAYNUM];
  float y[ARRAYNUM];
  float dst[ARRAYNUM];
  float sum=0;

  
  
  fp=fopen(filename,"r");                //ファイルを開く
  if(fp==NULL){
    printf("ファイルが開けません");
    return -1;
  }

 
  while(fgets(buffer,256,fp)){
    sscanf(buffer, "%f,%f", &x[i], &y[i]);
    i++;
  }
  
  /* for(j=1;j<i;j++){
    printf("%f\n",x[j]);
    printf("%f\n",y[j]);
    }*/

  dst[0]=kyori(x[i-1]-x[1],y[i-1]-y[1]);
  
  for(j=1;j<i-1;j++){
   dst[j]=kyori(x[j+1]-x[j],y[j+1]-y[j]);;
  }

  for(j=0;j<i-1;j++){
    sum+=dst[j];
  }
  
  printf("Distance of %s :%f\n",filename,sum);
  
}
