#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define ARRAYNUM 5000

float ret,min,tmp;
int num,num2,i=0,j,counter=0;
int flag[ARRAYNUM];
float x[ARRAYNUM];
float y[ARRAYNUM];
float dst[ARRAYNUM];
int neworder[ARRAYNUM];//greedyでできた新しい順番

float kyori(float a,float b);
void greedy(int num);


float kyori(float a,float b){  /*2点間の距離を計算*/
  ret=(float)sqrt((double)a*a+b*b);
  return ret;
  }

void greedy(int num){//各点に対して一番近い点を求める
  flag[num]=1;   //すでに使われてる場所に対しては1を入れる
  min=10000000; //minは呼び出し毎に初期化
  
  for(j=1;j<i;j++){
    if(flag[j]!=1){
      tmp=kyori(x[num]-x[j],y[num]-y[j]);
      if(tmp<min){
	min=tmp;
	num2=j;
      }
    }
  }
  
  printf("%d goes to %d and distance is %f\n",num,num2,min);
  counter++;
  dst[counter]=min;
  num=num2; 
  neworder[counter+1]=num-1;
  if(counter==(i-2)){//終点まで行ったら終わり
    return 0;
  }
  
  greedy(num);//次の点に対して再帰

}


int main(){
  FILE *fp,*fp2;
  char *filename="input_6.csv";
  char *filename2="solution_yours_6.csv";
  char buffer[256];
  char *tp1,*tp2;
  /* float x[ARRAYNUM];
     float y[ARRAYNUM];*/
  // float dst[ARRAYNUM];
  float sum=0;
  int startnum;
  /* int flag[ARRAYNUM];*/

  
  
  fp=fopen(filename,"r");                //ファイルを開く
  if(fp==NULL){
    printf("ファイルが開けません");
    return -1;
  }

 
  while(fgets(buffer,256,fp)){  //ファイルの読み込み
    sscanf(buffer, "%f,%f", &x[i], &y[i]);
    i++;
  }

  fclose(fp);

  startnum=1;  //はじめの点
  neworder[1]=startnum-1;
  greedy(startnum);
  dst[0]=kyori(x[startnum]-x[num],y[startnum]-y[num]);//終点から始点までの距離
  printf("%f\n",dst[0]);


  for(j=0;j<i-1;j++){//距離を求める
    sum+=dst[j];
  }


  printf("counter in %d\n",counter);
  printf("Distance of %s :%f\n",filename,sum);

  /*  for(j=1;j<i;j++){
  printf("%d\n",neworder[j]);
  }
  */

  fp2=fopen(filename2,"w");                //ファイルを開く
  if(fp2==NULL){
    printf("ファイルが開けません");
    return -1;
  }

  fprintf(fp2,"index\n");
  for(j=1;j<i;j++){
    fprintf(fp2,"%d\n",neworder[j]);
  }
 
  fclose(fp2);
 

}
